const usuarios_id="Usuarios";
function guardar_usuario() {
    let usuarios = []
    const usuario = {
        id: generarIdUsuario(),
        f_name: document.getElementById("f_name").value,
        l_name: document.getElementById("l_name").value,
        address1: document.getElementById("address1").value,
        address2: document.getElementById("address2").value,
        country: document.getElementById("country").value,
        city: document.getElementById("city").value,
        correo: document.getElementById("email").value,
        contraseña: document.getElementById("pass").value
        
    };
    if(localStorage.getItem(usuarios_id)){
        usuarios= JSON.parse(localStorage.getItem(usuarios_id));
    }
    let campos = validar();
    if(campos){
        nuevo(usuarios, usuario);
    }
    window.location="login.html";
}
function nuevo(usuarios, usuario){
    usuarios.push(usuario);
    localStorage.setItem (usuarios_id,JSON.stringify(usuarios));

    document.getElementById("f_name").value = "";
    document.getElementById("l_name").value = "";
    document.getElementById("address1").value = "";
    document.getElementById("address2").value = "";
    document.getElementById("country").value = "Costa Rica";
    document.getElementById("city").value = "";
    document.getElementById("email").value = "";
    document.getElementById("pass").value = "";
}

function validar(){
    if (document.getElementById("f_name").value == "" || document.getElementById("l_name").value == "" ||
    document.getElementById("address1").value == "" || document.getElementById("city").value == "" || 
    document.getElementById("email").value == "" || document.getElementById("pass").value == ""){
        alert('Rellene todos los campos');
        return false;
    }
    else{return true;}
    
};

 
function obtener_localstorage(nombre_persona){
    let persona = JSON.parse(localStorage.getItem(nombre_persona));
    console.log(persona);
}


function login(){
    const email = document.getElementById("email").value;
    const pass = document.getElementById('password').value;
    var texto = document.getElementById('submit_text');
    const restrese = "<p>Si no tiene cuenta de usuario <a href='registrarse.html'>registrese aqui.</a></p>";
    let user = localStorage.getItem("Usuarios");
    let data = JSON.parse(user);
    console.log(data);
    let inicio = false;
    if(email == "" || pass == ""){
        texto.innerHTML = 'Usuario invalido' + restrese;
    }
    if(data!=null){
        for (var i = 0; i < data.length; i++) {
            if(email == data[i].correo && pass == data[i].contraseña){
                sessionStorage.clear();
                seccionlocal(data[i]);
                texto.innerHTML = 'Iniciada' + restrese;
                inicio=true;
                break;
            }else{
                texto.innerHTML = 'Datos no coinsiden' + restrese;
            }
         }
    }else{
        texto.innerHTML = 'Datos no coinsiden' + restrese;
    }

     if(inicio){
        window.location="dashboard.html";
     }
    
}


function seccionlocal(user){
   let info = [user];
    sessionStorage.setItem("usuario", JSON.stringify(info));
}

function btn_ingresar(){
    var btn_in = document.getElementById('ingresar');
    var btn_in2 = document.getElementById('ingresar_footer');
    if (sessionStorage.length > 0) {
        btn_in.innerHTML='Cerrar Seccion'
        if(btn_in2 != null)
            btn_in2.innerHTML='Cerrar Seccion'
    }else{
        btn_in.innerHTML='Ingresar'
        if(btn_in2 != null)
            btn_in2.innerHTML='Ingresar'
    }
}
function comprobarSeccion(){
    var btn_in = document.getElementById('ingresar');
    var btn_in2 = document.getElementById('ingresar_footer');
    if (sessionStorage.length > 0) {
        btn_in.innerHTML='Dashboard'
        if(btn_in2 != null)
            btn_in2.innerHTML='Dashboard'
    }else{
        btn_in.innerHTML='Ingresar'
        if(btn_in2 != null)
            btn_in2.innerHTML='Ingresar'
    }
}
function comprobar_seccion(){
    if(sessionStorage.length == 1){
        window.location="dashboard.html";
    }else{
        window.location="login.html";
    }
}
function seccionDashboard(){
    var btn_in = document.getElementById('ingresar');
    sessionStorage.clear();
    window.location="login.html";
}

function bienvenida(){
    btn_ingresar()
    actualizarDashboard()
    var btn_in = document.getElementById('bienvenida');
    const nombre = JSON.parse(sessionStorage.getItem("usuario"));
    btn_in.innerHTML="Bienvendo <b>"+ nombre[0].f_name +"</b>, aqui podra encontrar el estado actual de sus Cambalaches y sus productos registrados."

}
function cambalaches(){
    window.location="cambalaches.html";
}
function cambalachesOnLoad(){
    comprobarSeccion();
    const listProductos = JSON.parse(localStorage.getItem("Productos"));
    const listUsuarios = JSON.parse(localStorage.getItem("Usuarios"));
    var productos = document.getElementById('productos');
    var txtProductos = "";
    if(listProductos != null){
        for (var i = 0; i < listProductos.length; i++){
            for (var e = 0; e < listUsuarios.length; e++){
                if(listProductos[i].creador == listUsuarios[e].id){
                    txtProductos= txtProductos+" <article class='product' style='cursor: pointer;' onclick=verProducto('"+listProductos[i].id+"')><div class='image'><img src='"+listProductos[i].imagen+"' alt='icon' class='img-product' width='100%' height='100%'></div><div style='margin-left: 10%;'><h1>"+listProductos[i].nombreProducto+"</h1><p>"+listUsuarios[e].f_name+"</p></div></article>";
                    
                }
            }
           
    
        }
    }else{
        txtProductos="No hay productos para mostrar"
        document.getElementById("cajaProductos").style.height = '250px';
    }
    
    productos.innerHTML= txtProductos;
}
function actualizarDashboard(){
    const listProductos = JSON.parse(localStorage.getItem("Productos"));
    const creador = JSON.parse(sessionStorage.getItem("usuario"));
    var productos = document.getElementById('productos');
    var txtProductos = "";
    if(listProductos != null){
        for (var i = 0; i < listProductos.length; i++){
            if(listProductos[i].creador == creador[0].id){
                txtProductos= txtProductos+"<article class='product'><div class='image'><img src='"+listProductos[i].imagen+"' alt='icon' class='img-product' width='100%' height='100%'></div><div class='info'><h2 class='ttle'><a href=''>"+listProductos[i].nombreProducto+"</a></h2><button type='button' class='btn btn-success' onclick=urlEditar('"+listProductos[i].id+"')>Editar</button><button type='button' class='btn btn-danger' onclick=Eliminar('"+listProductos[i].id+"')>Eliminar</button> </div></article> "
            }
    
        }
    }
    if(txtProductos == "")
        document.getElementById("productos").style.height = '250px';
    productos.innerHTML= txtProductos;
}

function urlEditar(id){
    window.location="nuevoProducto.html?product="+id;
    const listProductos = JSON.parse(localStorage.getItem("Productos"));
   
}
function mostrarEdit(){
    btn_ingresar();
    const product = window.location.search;
    const urlParams = new URLSearchParams(product);
    var producto = urlParams.get('product');
    if(urlParams.has('product')){
        console.log(producto);
        const listProductos = JSON.parse(localStorage.getItem("Productos"));
        for (var i = 0; i < listProductos.length; i++){
            if(listProductos[i].id == producto){
                document.getElementById('nombreProducto').value = listProductos[i].nombreProducto;
                document.getElementById('descripcion').value = listProductos[i].descripcion;
                document.getElementById('imagen').value = listProductos[i].imagen;
                document.getElementById('busco').value = listProductos[i].busco;
            }
        }

    };
}

function Eliminar(id){
    const listProductos = JSON.parse(localStorage.getItem("Productos"));
    for (var i = 0; i < listProductos.length; i++){
        if(listProductos[i].id == id){
            listProductos.splice(i, i);
            if(i == 0){
                listProductos.shift();
            }
        }
    }
    localStorage.removeItem("Productos");
    if(listProductos.length != 0){
        localStorage.setItem ("Productos",JSON.stringify(listProductos));
    }

    location. reload();
}


function nuevoProducto(){
    const product = window.location.search;
    const urlParams = new URLSearchParams(product);
    var producto = urlParams.get('product');
    let listProductos=[];
    if(urlParams.has('product')){
        console.log(producto);
        listProductos = JSON.parse(localStorage.getItem("Productos"));
        for (var i = 0; i < listProductos.length; i++){
            if(listProductos[i].id == producto){
                const nombreProducto= document.getElementById("nombreProducto").value;
                const descripcion= document.getElementById("descripcion").value;
                const imagen= document.getElementById("imagen").value;
                const busco= document.getElementById("busco").value;
                listProductos[i].nombreProducto = nombreProducto;
                listProductos[i].descripcion = descripcion;
                listProductos[i].imagen = imagen;
                listProductos[i].busco = busco;
            }
        }
    }else{
        const IdProductos = generarIdProducto();
        const Creador = JSON.parse(sessionStorage.getItem("usuario"));
        const IdCreador = Creador[0].id;
        const producto = {
        id : IdProductos,
        creador : IdCreador,        
        nombreProducto: document.getElementById("nombreProducto").value,
        descripcion: document.getElementById("descripcion").value,
        imagen: document.getElementById("imagen").value,
        busco: document.getElementById("busco").value
        }
        if(localStorage.getItem("Productos")){
            listProductos= JSON.parse(localStorage.getItem("Productos"));
        }
        listProductos.push(producto);
    }
  
    localStorage.setItem ("Productos",JSON.stringify(listProductos));

    window.location="dashboard.html";
}

function generarIdProducto(){
    var caracteres = "abcdefghijkmnpqrtuvwxyzABCDEFGHJKMNPQRTUVWXYZ2346789";
    var codigo = "";
    let vertifyId = [];
    while (true){
        for (i=0; i<4; i++) codigo +=caracteres.charAt(Math.floor(Math.random()*caracteres.length)); 
        console.log(codigo)
        if(localStorage.getItem("Productos")){
            vertifyId= JSON.parse(localStorage.getItem("Productos"));
        }else{
            return codigo;
        }
        
        for (var i = 0; i < vertifyId.length; i++){
            if(vertifyId[i].id != codigo){
                return codigo;
            }
        }
    }
    
    
}
function generarIdUsuario(){
    var caracteres = "abcdefghijkmnpqrtuvwxyzABCDEFGHJKMNPQRTUVWXYZ2346789";
    var codigo = "";
    let vertifyId = [];
    while (true){
        for (i=0; i<4; i++) codigo +=caracteres.charAt(Math.floor(Math.random()*caracteres.length)); 
        console.log(codigo)
        if(localStorage.getItem("Productos")){
            vertifyId= JSON.parse(localStorage.getItem("Productos"));
        }else{
            return codigo;
        }
        
        for (var i = 0; i < vertifyId.length; i++){
            if(vertifyId[i].id != codigo){
                return codigo;
            }
        }
    }
    
    
}

function carrucel(){
    comprobarSeccion();
    const listProductos = JSON.parse(localStorage.getItem("Productos"));
    var carrucel = document.getElementById('carrucel');
    var txt = "";
    if(listProductos.length <= 6 && listProductos.length > 0){
        if(listProductos.length%2==0){
            
            for (var i = 0; i < listProductos.length; i++){
                if(i%2==0 || i == 0 ){
                    if(i != 6){
                        var num = (listProductos.length-1)- i;
                        if(i==0){
                            txt = txt + " <div class='carousel-item active' ><div class='row' style='width: 95%; margin: auto; '> <div class='col-lg' style='text-align: center; '> <article class='container-img-2' style='cursor: pointer;' onclick=verProducto('"+listProductos[num].id+"')><img src='"+listProductos[num].imagen+"' alt='item' style='margin: auto; vertical-align: middle;' width='100%' height='100%' ></article><p>"+listProductos[num].nombreProducto+"</p></div><!-- /.col-lg-4 --> <div class='col-lg'  style='text-align: center;'> <article class='container-img-2'  style='cursor: pointer;' onclick=verProducto('"+listProductos[num-1].id+"')><img src='"+listProductos[num-1].imagen+"' alt='item' style='margin: auto; vertical-align: middle;' width='100%' height='100%' ></article><p>"+listProductos[num-1].nombreProducto+"</p>  </div> </div></div>"
                        }else{
                            txt = txt + " <div class='carousel-item' ><div class='row' style='width: 95%; margin: auto; '> <div class='col-lg' style='text-align: center; '> <article class='container-img-2'  style='cursor: pointer;' onclick=verProducto('"+listProductos[num].id+"')><img src='"+listProductos[num].imagen+"' alt='item' style='margin: auto; vertical-align: middle;' width='100%' height='100%' ></article><p>"+listProductos[num].nombreProducto+"</p></div><!-- /.col-lg-4 --> <div class='col-lg'  style='text-align: center;'> <article class='container-img-2'  style='cursor: pointer;' onclick=verProducto('"+listProductos[num-1].id+"')><img src='"+listProductos[num-1].imagen+"' alt='item' style='margin: auto; vertical-align: middle;' width='100%' height='100%' ></article><p>"+listProductos[num-1].nombreProducto+"</p>  </div> </div></div>"
                        }
                        
                    }else{
                        break;
                    }
                    
                }
                
            }
            
        }else{
            var decrement = listProductos.length;
            for (var i = 0; i < listProductos.length; i++){
                if(i%2==0 || i == 0 ){
                    if(i != 6){
                        var num = (listProductos.length-1)- i;
                        if(decrement>=2){
                            if(i==0){
                                txt = txt + " <div class='carousel-item active' ><div class='row' style='width: 95%; margin: auto; '> <div class='col-lg' style='text-align: center; '> <article class='container-img-2'  style='cursor: pointer;' onclick=verProducto('"+listProductos[num].id+"')><img src='"+listProductos[num].imagen+"' alt='item' style='margin: auto; vertical-align: middle;' width='100%' height='100%' ></article><p>"+listProductos[num].nombreProducto+"</p></div><!-- /.col-lg-4 --> <div class='col-lg'  style='text-align: center;'> <article class='container-img-2'  style='cursor: pointer;' onclick=verProducto('"+listProductos[num-1].id+"')><img src='"+listProductos[num-1].imagen+"' alt='item' style='margin: auto; vertical-align: middle;' width='100%' height='100%' ></article><p>"+listProductos[num-1].nombreProducto+"</p>  </div> </div></div>"
                            }else{
                                txt = txt + " <div class='carousel-item' ><div class='row' style='width: 95%; margin: auto; '> <div class='col-lg' style='text-align: center; '> <article class='container-img-2'  style='cursor: pointer;' onclick=verProducto('"+listProductos[num].id+"')><img src='"+listProductos[num].imagen+"' alt='item' style='margin: auto; vertical-align: middle;' width='100%' height='100%' ></article><p>"+listProductos[num].nombreProducto+"</p></div><!-- /.col-lg-4 --> <div class='col-lg'  style='text-align: center;'> <article class='container-img-2'  style='cursor: pointer;' onclick=verProducto('"+listProductos[num-1].id+"')><img src='"+listProductos[num-1].imagen+"' alt='item' style='margin: auto; vertical-align: middle;' width='100%' height='100%' ></article><p>"+listProductos[num-1].nombreProducto+"</p>  </div> </div></div>"
                            }
                            decrement = decrement - 2;
                        }else{
                            if(i==0){
                                txt = txt + " <div class='carousel-item active' ><div class='row' style='width: 95%; margin: auto;'> <div class='col-lg' style='text-align: center; '> <article class='container-img-2'  style='cursor: pointer;' onclick=verProducto('"+listProductos[num].id+"')><img src='"+listProductos[num].imagen+"' alt='item' style='margin: auto; vertical-align: middle;' width='100%' height='100%' ></article><p>"+listProductos[num].nombreProducto+"</p></div><!-- /.col-lg-4 --> <div class='col-lg'  style='text-align: center;'> <article class='container-img-2' ><img src='img/icon.png' alt='item' style='margin: auto; vertical-align: middle;' width='80' height='70' ></article><p>Vacio</p>  </div> </div></div>"
                            }else{
                                txt = txt + " <div class='carousel-item' ><div class='row' style='width: 95%; margin: auto;'> <div class='col-lg' style='text-align: center; '> <article class='container-img-2'  style='cursor: pointer;' onclick=verProducto('"+listProductos[num].id+"')><img src='"+listProductos[num].imagen+"' alt='item' style='margin: auto; vertical-align: middle;' width='100%' height='100%' ></article><p>"+listProductos[num].nombreProducto+"</p></div><!-- /.col-lg-4 --> <div class='col-lg'  style='text-align: center;'> <article class='container-img-2' ><img src='img/icon.png' alt='item' style='margin: auto; vertical-align: middle;' width='80' height='70' ></article><p>Vacio</p>  </div> </div></div>"
                            }
                        }
                       
                        
                       
                        
                    }else{
                        break;
                    }
                    
                }
                
            }
        }
    }
    carrucel.innerHTML= txt;
}

function verProducto(id){
    window.location="producto.html?product="+id;
}
function verProductoOnLoad(){
    comprobarSeccion();
    const product = window.location.search;
    const urlParams = new URLSearchParams(product);
    var producto = urlParams.get('product');
    if(urlParams.has('product')){
        console.log(producto);
        const listProductos = JSON.parse(localStorage.getItem("Productos"));
        const listcreadores = JSON.parse(localStorage.getItem("Usuarios"));
        for (var i = 0; i < listProductos.length; i++){
            if(listProductos[i].id == producto){
                for (var e = 0; e < listcreadores.length; e++){
                    if(listProductos[i].creador == listcreadores[e].id){
                        var element = document.getElementById('product');
                        var txt = "<div class='image' ><img src='"+listProductos[i].imagen+"' alt='icon' class='img-product' width='100%' height='100%'></div><div style=' background-color: white; padding-left: 6%;'><div style='margin: auto; width: 90%; word-wrap: break-word;'><h1 style='padding-top: 5%; font-size: 38px;'>"+listProductos[i].nombreProducto+"</h1><p style='padding-top: 1%; font-size: 16px;'>Ofrecido por: <a style='font-size: 16px;' href=''>"+listcreadores[e].f_name+"</a></p><h2 style='padding-top: 7%; font-size: 17px;'>DESCRIPCIÓN</h1><p style='padding-top: 1%; font-size: 16px;'>"+listProductos[i].descripcion+"</a></p><h2 style='padding-top: 7%; font-size: 17px;'>BUSCO</h1><p style='padding-top: 1%; font-size: 16px;'>"+listProductos[i].busco+"</a></p><button type='button' class='btn btn-secondary btn-lg' style='width: 140px; font-size: 16px; margin-top: 14%;'>Agregar</button></div></div>"
                        element.innerHTML = txt;
                    }
                }
            }
        }

    };
}